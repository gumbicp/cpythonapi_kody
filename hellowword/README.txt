Przykład pierwszy "Witaj w świecie rozszerzeń Pythona w języku C"
Dostęp  w Pythonie do kodu napisanego w języku C.

Mamy biblioteki Pythona napisane w C w kodzie programu Blender i dostęp do kodu w
języku C jest ważnym aspektem komunikowania się skryptów Pythona z takim typem bibliotek.
Jest to temat nie łatwy, ze względu na problemy z kompilacją języka C do biblioteki
współdzielonej z godnej z interpreterem Pythona.Chodzi tu o wykorzystanie tej samej architektury,
rozmiaru słowa, kompilatora itp. Na różnych systemach mogą występować niewielkie różnice
w procesach kompilacji takich kodów.

Najlepiej zaczynać od prostych przykładów i pisać, pisać , testować ,
pisać więc zaczynamy od tego prostego przykładu. Pierwszym plikiem który musimy napisać jest witaj.c.

Użycie własnego modułu w blenderze.

I tu najpierw sprawdzamy wersje blendera i która wersja Pythona jest w nim użyta.
Blender 2.70 używa Pythona 3.3. Informacje te są widoczne w konsoli blendera.

W tym przypadku kompilujemy pliki przez Python 3. Blender 2.71 ma już Pythona 3.4,
więc nasz moduł nie będzie działał w konsoli blendera czy skryptach BPY. I trzeba
prze kompilować wszytko przez Python 3.4. I adekwatnie zależnie od wersji Pythona w
blenderze, kompilujemy taki moduł tą samą wersją języka.

Gdy kompilujemy nasz moduł przez Python 3.4 setup.py install dla wersji Blendera 2.71,
będzie on widoczny w systemie dla zwykłych programów pisanych w tym języku.
Ale niestety Blender go nie znajdzie ( w większości przypadków ). Problem jest tego typu
że moduły Pythona są przez Blender ładowane z bibliotek Pythona wbudowanego w program.
Aby używać naszego modułu wystarczy skopiować nasz plik wynikowy *.so do folderu
/blender-2.71-linux-glibc211-x86_64/2.71/python/lib/python3.4/.

No i w końcu o to nam chodziło i nasz moduł działa.