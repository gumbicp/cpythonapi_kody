""" 
 plik: setup.py

 Do kompilacji naszego rozszerzenia Python z C.
 Tworzy moduł spam z pliku witaj.c,
 wynikiem działania pliku jest spam.cpython-34m.so 
 Nazwa może się trochę różnić na innych systemach.
 Wersje Pythona możecie używać dowolne dla potrzeb 
 Blendera 2.71 musi być kompilowane python3.4

 Kompilujemy moduł komendą 
                dla rozszerzenia na miejscu
		python3.4 setup.py build_ext --inplace
                utworzy nam foldel build z podfolderami
                python3.4 setup.py build_ext 
                zbuduje nam paczke zip lub tar do instalacji na innych komputerach
                python3.4 setup.py sdist
                dostępne formaty plikow 
                python3.4 setup.py bdist  --help-formats
                zainstaluje w systemie
                python3.4 setup.py install 

distutils jest zbiorem modułów bardzo prostych i pomocnych przy instalacjach,
kompilacji i dystrybucji modułów Pythona w sieć z możliwością rejestracji nawet
w oficjalnych bibliotekach Pythona. Ma naprawdę sporo możliwości i odsyłam tu do
czytania API na stronie https://docs.python.org/3/distutils/index.html

Przykład kodu spammodule.c ze strony 
https://docs.python.org/3/extending/index.html#extending-index
#
Przykładowe wyjście gdy nie ma błędów:


running build_ext
building 'witaj' extension
creating build
creating build/temp.linux-i686-3.4
gcc -pthread -Wno-unused-result -Werror=declaration-after-statement -DNDEBUG -g -fwrapv -O3 -Wall -Wstrict-prototypes -fPIC -I/usr/local/include/python3.4m -c witaj.c -o build/temp.linux-i686-3.4/witaj.o
gcc -pthread -shared build/temp.linux-i686-3.4/witaj.o -o /home/lapgpc/Projects/pythoncApi/nauka/hellowword/witaj.cpython-34m.so
"""
from distutils.core import setup, Extension

module1 = Extension("witaj", sources = ["witaj.c"])

setup(name="modul witaj",
	  version = '1.0',
	  description = 'Opis tego modułu',
	  ext_modules = [module1])
