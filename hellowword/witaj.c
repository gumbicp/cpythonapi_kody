/*
 * plik: witaj.c 
 * 
 * Przykladowy moduł rozszerzenia Pythona napisany w języku C.
 * 
 * @author gumbicp
 * @version 1.0
 * @date 2014 lipiec 09
 */
 
// Na starcie includujemy Python.h

#include <Python.h>


/*
 *  Przykładowa funkcja powitanie(name),
 * wypisze nam przywitanie z podanym imieniem.
 * 
 *  */
static PyObject *
py_powitanie(PyObject* self, PyObject* args)
{
	// dla przysłanego imienia
	const char* name;
	
	//warunek konieczny 
	if(!PyArg_ParseTuple(args, "s", &name))
		return NULL;
	
	// działania w funkcji	
	printf("Witaj %s w module rozszerzającym python`a w języku C \n", name);
	
	// Python gdy nie ma komendy return z wraca zawsze None.
	Py_RETURN_NONE;
}
/*
 *  Tablica z metodami modułu 
 * */
static PyMethodDef WitajMethods[] = 
{
	{"powitanie", py_powitanie, METH_VARARGS, "Przywitanie kogos"},
	{NULL, NULL, 0, NULL}
};

/* 
 * Struktura Modułu 
 * */
static struct PyModuleDef witajmodule = {
	PyModuleDef_HEAD_INIT,
	"witaj" , 				/* Nazwa Modułu */
	"witaj modul testowy",  /* łańcuch znaków z dokumentacja może być NULL */
	-1, 					/* Długość danych ze stanem interpretera lub -1*/
	WitajMethods			/* Tablica z metodami */ 
};
/* 
 * Funkcja inicjująca moduł 
 * */
PyMODINIT_FUNC
PyInit_witaj(void)
{
	return PyModule_Create(&witajmodule);
}
