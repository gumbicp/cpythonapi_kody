dokumentacja = """
 plik: setup.py

 Do kompilacji naszego rozszerzenia Python z C.
 Tworzy moduł spam z pliku spammodule.c,
 wynikiem działania pliku jest spam.cpython-34m.so 
 Nazwa może się trochę różnić na innych systemach.
 Wersje Pythona możecie używać dowolne dla potrzeb 
 Blendera 2.71 musi być kompilowane python3.4

 Kompilujemy moduł komendą 
                dla rozszerzenia na miejscu
				python3.4 setup.py build_ext --inplace
                utworzy nam foldel build z podfolderami
                python3.4 setup.py build_ext 
                zbuduje nam paczke zip lub tar do instalacji na innych komputerach
                python3.4 setup.py sdist
                dostępne formaty plikow 
                python3.4 setup.py bdist  --help-formats
                zainstaluje w systemie
                python3.4 setup.py install 

distutils jest zbiorem modułów bardzo prostych i pomocnych przy instalacjach,
kompilacji i dystrybucji modułów Pythona w sieć z możliwością rejestracji nawet
w oficjalnych bibliotekach Pythona. Ma naprawdę sporo możliwości i odsyłam tu do
czytania API na stronie https://docs.python.org/3/distutils/index.html

Przykład kodu spammodule.c ze strony 
https://docs.python.org/3/extending/index.html#extending-index

"""

from distutils.core import setup, Extension

# klasa Extension dla plików rozszerzeń C : konstruktor przyjmuje tutaj
# dwa parametry nazwę naszego modułu i plik źródłowy 
# można też dodawać opcję typu flagi kompilacji ścieżki do plików
# nagłówkowych bin-arek *.so(ddl) itp itp wymaganych przy kompilacji plików C
module1 = Extension("spam", sources = ['spammodule.c'])

# setup ustawia nam metadane (informacje o module)
# i co jest rozszerzeniem (naszym źródłem kodu C) czyli parametr ext_modules
setup(name = 'spam',
      version = '1.0',
      description = 'This is smpam module',
      author = 'Jas Fasola',
      author_email = 'fasolaland@glupoty.ppy',
      url = 'http://www.fasolaland.ppy',
      long_description = dokumentacja,
      ext_modules = [module1])