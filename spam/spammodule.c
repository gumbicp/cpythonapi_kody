/*
 * Code from https://docs.python.org/3/extending/index.html#extending-index
 * Extending and Embedding the Python Interpreter
 */
#include <Python.h>

/* obiekt dla naszego typu błędu */
static PyObject * SpamError;

/*
 *  Funkcja wywołująca polecenia systemowe w konsoli.
 * Wszystkie tego typu funkcje muszą być statyczne i zwracać typ
 * wskaźnikowy PyObject. Przyjmuje standardowo dwa obiekty 
 * - self : zaczep dla obiektu wywołującego funkcje
 * - args : argumenty przysłane funkcji.
 *  */
static PyObject * spam_system(PyObject * self, PyObject * args){
    /* zmienna dla naszej komendy przysłanej */
    const char * command;
    /* zmienna do obsługi błędów - 0 brak błędu */
    int sts;
    
    /*
     *  Warunek parsujący argumenty funkcji do krotki
     * Gdy parsowanie się nie uda zwróci NULL czyli wywali nam błąd
     * @args argumenty do parsowania
     * @string typy argumentów
     * @Adresy zmiennych typu C
     */
    if(!PyArg_ParseTuple(args, "s", &command))
        return NULL;
    /* komenda system wysyła nam przysłane polecenie do konsoli */
    sts = system(command);
    /* Jeżeli system zwrócił nam błąd (nie zerową wartość) 
     * to ustawiamy komunikat błędu i zwracamy NULL
     */
    if(sts > 0){
        PyErr_SetString(SpamError, "System command failed.. sory Vinetu you have to learn something about your system  - he dont like d.... peoples");
        return NULL;
    }
    /* Funkcja zawsze musi coś zwracać tu int równy 0 */
    return PyLong_FromLong(sts);
    
}
/*
 * Tablica metod modułu . Na końcu zawsze jest wymagany wartownik.
 * @string nazwa metody w module docelowym
 * @nazwa metody w tym pliku
 * @Makro wymagane i przeważnie zawsze w tej postaci
 * @Dokumentacja funkcji
 **/
static PyMethodDef SpamMethods[] = {
    {"system", spam_system, METH_VARARGS, "Execute a shell command"},
    {NULL, NULL, 0, NULL} /* wartownik */

};

/*
 * struktura modułu 
 **/
static struct PyModuleDef spammodule = {
    PyModuleDef_HEAD_INIT,
    "spam",                 /* Nazwa Modułu */
    "Dokumentacja modułu krótka",  /* łańcuch znaków z dokumentacja może być NULL */
    -1,                     /* Długość danych ze stanem interpretera lub -1*/
    SpamMethods             /* Tablica z metodami */ 
};

/*
 * Funkcja inicjująca nasz moduł.
 **/
PyMODINIT_FUNC PyInit_spam(void){
    /* wskaźnik na obiekt modułu */
    PyObject * m;
    
    /* tworzymy moduł za wysyłając tam strukturę modułu */
    m = PyModule_Create(&spammodule);
    if(m == NULL){
        return NULL;
    }
    /* Rozszerzamy moduł o obsługę naszego błędu 
     * Tworzy się tu naszą klasę error która będzie w strukturze 
     * klas błędów Pythona
     */
    SpamError = PyErr_NewException("spam.error", NULL,NULL);
    /* Podnosimy instancję ( wyjaśnienie tego będzie w przykładach następnych 
     *  Są to operacje dosyć mocno narażone na generowanie fatalnych błędów )
     */
    Py_INCREF(SpamError);
    /* dodajemy obiekt do modułu */
    PyModule_AddObject(m, "error", SpamError);
    return m;
}